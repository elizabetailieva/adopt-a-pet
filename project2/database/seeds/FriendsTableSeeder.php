<?php

use Illuminate\Database\Seeder;
use \App\Friend;

class FriendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['Cats', 'Dogs', 'Children'];

        foreach($array as $name) {
            $friend = new Friend;
            $friend->name = $name;
            $friend->save();
        }
    }
}
