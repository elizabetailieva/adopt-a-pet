<?php

use App\City;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\DatabaseController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\AdminController;
use App\Http\Middleware\CheckRole;


Route::get('/', 'GeneralController@welcome')->name('welcome');

Auth::routes();

// Socialite Routes
Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');

Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

Route::get('/home', 'HomeController@index')->name('home');   

//Edit user profile

Route::get('/user', 'HomeController@editProfileUser')->name('editProfileUser');

Route::post('/user', 'DatabaseController@editProfile')->name('editProfilePost');

//Add a pet

Route::get('/user/pet', 'HomeController@myPets')->name('myPets');

Route::get('/user/add/pet', 'HomeController@addAPet')->name('addAPet');

Route::post('/user/pet', 'DatabaseController@savePet')->name('savePet');

//Image Upload routes 

Route::post('/images-save', 'UploadImagesController@uploadSubmit')->name('addPhotosPet');

Route::get('/pets', 'HomeController@listPets')->name('listPets');

Route::get('/pets/location', 'HomeController@filterPetsByCity')->name('petsLocation');

//Filter pets

Route::get('/filter', 'PetsFilterController@filter')->name('filter');

//Pet profile

Route::get('/pet', 'GeneralController@petID')->name('petID');

Route::get('/pet/details', 'GeneralController@petDetails')->name('petDetails');

//Inquire a pet

Route::post('/inquirepet', 'MailController@inquirePet')->name('inquirePet');

//Admin panel
Route::get('/adminpanel')->name('adminpanel')->uses('AdminController@showAdminPanel');

//Admin panel pet-options

Route::get('/update/{id}')->name('update')->uses('AdminController@updateStatus');

Route::get('/approved/{id}')->name('approved')->uses('AdminController@approvePost');

Route::get('/deny/{id}')->name('deny')->uses('AdminController@deny');

Route::get('/delete/{id}')->name('delete')->uses('AdminController@delete');




