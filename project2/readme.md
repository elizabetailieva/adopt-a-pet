# Installation

Please check the official laravel installation guide for server requirements before you start.

### Clone the repository

```
git clone git@gitlab.com:elizabetailieva/adopt-a-pet.git
```

### Setting up the project

Switch to the repo folder

Install all the dependencies using composer

```
composer install
```

Copy the example env file and make the required configuration changes in the .env file

```
cp .env.example .env
```

Setup mailserver to test sending emails

Fill this fields in .env to test Socialite API

* FACEBOOK_ID=********
* FACEBOOK_SECRET=****************
* FACEBOOK_URL=http://localhost:8000/auth/facebook/callback

Run the database migrations (Set the database connection in .env before migrating)
```
php artisan migrate
```

Run the database seeder
```
php artisan db:seed
```

Delete the public/storage broken link, and run the command
```
php artisan storage:link
```

To access the admin panel you need to register user, and change it's role from 0 to 1 in the database

You are all set!






