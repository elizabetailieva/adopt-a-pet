$(function(){

//Логика за ресетирање на филтрите на рефреш
  

    $.each($('#filtersContainer').find('option:selected'), function(key, value){
        var selectedSelect = $(this).parent();
        selectedSelect[0].selectedIndex = 0;
    });
    $.each($('.checkboxes:checkbox:checked'), function(key, value){               
         $(this).prop('checked', false);
     })

//Логика за креирање на дивови со име на филтрерот кој е одбран
    var selectText = [];
    var filterText = [];
    function unique(list) {
        var result = [];
        $.each(list, function(i, e) {
        if ($.inArray(e, result) == -1) result.push(e);
        });
        return result;
    };

    //дел за селектите

    $(document).on('change', 'select', function(){
        var selectTextString = $(this).find('option:selected').text();
    
        if($.inArray(selectTextString, selectText) == -1){
            selectText.push(selectTextString);
            var newDiv = $('.filter-span').last().clone();
            newDiv.find('.filter-form-text').text(selectTextString);
            $('.filter-cards-list').append(newDiv);
            newDiv.css('display', 'block');
            filterText.push(newDiv.text().trim());
        }
    
    });

    //дел за чекбоксот

    $('.checkboxes').on('click', function(){
        var checkboxText = $(this).attr('data-value');

        if($(this). prop("checked") == true){
            if($.inArray(checkboxText, selectText) == -1){
                selectText.push(checkboxText);
                var newDiv = $('.filter-span').last().clone();
                newDiv.find('.filter-form-text').text(checkboxText);
                $('.filter-cards-list').append(newDiv);
                newDiv.css('display', 'block');
                filterText.push(newDiv.text().trim());
            }
        }
    });


//Логика за ресетирање на филтрите при гасење на дивовите на навбар

    $(document).on('click', '.filter-form-close', function(){
        $(this).closest('.filter-span').hide();
        var turnOff =  $(this).parent().prev().text();
        var allSelects = $('#filtersContainer').find('option:selected');

        $.each(allSelects, function(key, value){
            if(turnOff == value.text) {
                var selectedSelect = $(this).parent();
                selectedSelect[0].selectedIndex = 0;
            }
        });

        var allCheckboxes = $('.checkboxes:checkbox:checked');

        $.each(allCheckboxes, function(key, value){
            var checkboxText = $(this).attr('data-value');
            if(turnOff == checkboxText) {
                $(this).prop('checked', false);
            }
        });
    }); 
});