$(function () {

//Close the navbar modal if screen size is larger than mobile

function modalClose(){
    if($(window).width() > 768){
        $('#Navbar_Modal').hide();    
        $('.modal-backdrop').remove();
    }
    else {
        if($('body').hasClass('modal-open')){
            $('body').removeClass('modal-open');
        }
    }
}
    
window.addEventListener('resize', modalClose);

//Превенција од отварање на слика во browser без да се аплоадира

    window.addEventListener("dragover",function(e){
    e = e || event;
    e.preventDefault();
    },false);

    window.addEventListener("drop",function(e){
    e = e || event;
    e.preventDefault();
    },false);

//Логика за вртење на стрелчето од менито
    $('#button').click(function() {    
        $("#strelce").toggleClass("fa-angle-up fa-angle-down"); 
    });

//Логика за прикажување на user profile shortcut
    $('#user_avatar').click(function() {    
        $("#user_links").slideToggle(); 
        if($('#user_avatar').hasClass('reverse-color')){
            $('#user_avatar').removeClass('reverse-color');
        } else {
            $('#user_avatar').addClass('reverse-color');
        }
    });

    
});