$(function(){
    
    $(document).on('mouseenter', '.pet-card-element', function() {
        $(this).find('.petFirstLayer').slideToggle(500);
        $(this).find('.petSecondLayer').slideToggle(500);
    })

    $(document).on('mouseleave', '.pet-card-element', function() {
        $(this).find('.petFirstLayer').slideToggle(500);
        $(this).find('.petSecondLayer').slideToggle(500);
    })

    $(document).on('click', '.click-share', function(){
        $(this).find('.icon-share-close').toggleClass("fa-share fa-times")
        $(this).parent().toggleClass("white-bg purple-bg");
        $(this).siblings('.card-caption').children('.detailsList').toggle();
        $(this).siblings('.card-caption').children('.second-layer-pet-name').toggle();
        $(this).siblings('.card-caption').children('.share-pet-name').toggle();
        $(this).siblings('.card-caption').children('.shareList').toggle();
    });

});