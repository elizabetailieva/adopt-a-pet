@extends('master') 
@section('content')
<div class="container-fluid">
    <div class="add-a-pet-body">
        <div class="card">
            <div class="card-section card-section_constrainedPadLg">
                <div class="row">
                    <div class="hidden-xs col-sm-12 col-md-12">
                        <ul class="list-unstyled list-inline">
                            <li><a class="item-link " href="{{route('editProfileUser')}}">За мене</a></li>
                            <li><a class="item-link active" href="{{route('myPets')}}">Моите миленичиња</a></li>
                            <li><a class="item-link" href="{{route('addAPet')}}">Додади милениче</a></li>
                        </ul>
                        <div class="separator"></div>
                    </div>
                    <div class="col-xs-12 hidden-sm hidden-md hidden-lg">
                        <ul class="list-unstyled list-inline">
                            <li><a class="item-link-small " href="{{route('editProfileUser')}}">За мене</a></li>
                            <li><a class="item-link-small active" href="{{route('myPets')}}">Моите миленичиња</a></li>
                            <li><a class="item-link-small" href="{{route('addAPet')}}">Додади милениче</a></li>
                        </ul>
                        <div class="separator"></div>
                    </div>
                </div>

                @if(count($pets) != 0)
                <div class="row">
                    @foreach($pets as $pet)
                        <div class="col-md-4 col-sm-6 pet-card-element u-vr10x">
                            <div class="pet-card white-bg">
                                <div class="media">
                                    <a href="{{route('petID', ['id' => encrypt($pet->id)])}}">
                                        <div class="media-image">
                                            <img class="img-responsive card-image" src="{{asset('storage/'.$pet->uploads()->first()->filename)}}">
                                        </div>
                                    </a>
                                    <div class="white-clip">
                                    </div>
                                </div>
                                <div class="card-caption">
                                    <p class="pet-name">{{$pet->name}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @else 
                <div class="row">
                    <div class="col-md-12">
                        <h4>Моментално немате внесено миленичиња</h4>
                    </div>
                </div>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection