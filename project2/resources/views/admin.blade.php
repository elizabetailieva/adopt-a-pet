@extends('master') 
@section('content')
    <div class="container-fluid petsForApprove">
        <div class="row">
            <div class="table-responsive">
                <table class="table">
                    <thead class="tableHead">
                        <tr>
                            <th>Име</th>
                            <th>Тип</th>
                            <th>Сопственик</th>
                            <th>Град</th>
                            <th>Години</th>
                            <th>Големина</th>
                            <th>Пол</th>
                            <th class="description-column">Опис</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="table">
                        @foreach($pets as $pet)
                            <tr>
                                <td>
                                    {{ $pet->name }}
                                </td>
                                <td>
                                    @if($pet->type == 'cat') 
                                    Маче 
                                    @elseif($pet->type == 'dog') 
                                    Куче 
                                    @endif
                                </td>
                                <td>
                                    {{ $pet->user->first_name }}
                                </td>
                                <td>
                                    {{ $pet->city->name }}
                                </td>
                                <td>
                                    @if($pet->age == 'young')
                                    Младо 
                                    @elseif($pet->age == 'adult') 
                                    Возрасно 
                                    @elseif($pet->age == 'old') 
                                    Старо 
                                    @endif
                                </td>
                                <td>
                                    @if($pet->size == 'sm')
                                    Мал раст
                                    @elseif($pet->size == 'md') 
                                    Среден раст
                                    @elseif($pet->size == 'lg') 
                                    Голем раст
                                    @endif
                                </td>
                                <td>
                                    @if($pet->gender == 'm') 
                                    Машко 
                                    @else 
                                    Женско 
                                    @endif
                                </td>
                                <td>{{ $pet->description }}</td>
                                <td>
                                    @if($pet->approved == 1)
                                    Одобрено
                                    @else
                                    Неодобрено
                                    @endif
                                </td>

                                <td><a href="{{ route('update',['id' => $pet->id]) }}"class="btn btnStatus">Опции</a> </td>
                            </tr>  

                        @endforeach  
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
