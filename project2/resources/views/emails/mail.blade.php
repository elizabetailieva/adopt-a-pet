<!DOCTYPE html>

<html>

    <head>
        <link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Patrick+Hand" rel="stylesheet">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style>
           body{
                font-family: "Cabin", sans-serif;
                font-size: 16px; 
                background-color: #efeef1;
           }
            .email-container {
                height: 100vh;
                display: flex;
                align-items: center;
                justify-content: center;
            }

            .mail-style-div {
                background-color: #6504b5;
                color: #ffffff;
                border-radius: 10px;
                height: auto;
                width: 60vw;
                position: relative;
            }

            .mail-heading {
                margin-left: 50px;
                margin-right: 50px;
                display: flex;
                align-items: center;
            }

            .split-mail{
                height: 5px;
                background-color: #efeef1;
            }

            .mail-body {
                margin-top: 15px;
                margin-left: 50px;
                margin-right: 50px;
            }

            .roundImage img {
                border-radius: 50%;
                height: 120px;
                width: 120px;
                border: 3px solid white;
                margin-top:20px;
                margin-bottom: 20px;
            }
            .text-left-of-picture > h3,
            .text-left-of-picture > h4{
                margin-left: 20px;
            }


        </style>
    </head>
    <body>

        <div class="container email-container">
                   
            <div class="row mail-style-div">      
               
                <div class="col-md-12 mail-heading">
                    <div class="roundImage"><img src="{{asset('storage/photos/'.$image)}}"></div>
                    <div class="text-left-of-picture">
                    <h3>Здраво {{$recipient_name}}</h3>
                    <h4>Дојдовте еден чекор поблиску до вдомување на {{$pet_name}}!</h4>
                    </div>
                </div>
                <div class="col-md-12 split-mail"></div>
                <div class="col-md-12 mail-body">
                    <p>Добивте порака од <span class="purple">{{$sender_name}} од {{$city_name}}</span></p>
                    <p>Порака:  <span class="purple">{{$content}}</span></p>
                    <p>Телефон за контакт:  <span class="purple">{{$telephone}}</span></p>
                </div>
            </div>
        </div>
    </body>
</html>