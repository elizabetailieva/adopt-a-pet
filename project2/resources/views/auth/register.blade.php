<!DOCTYPE html>

<html>

<head>
    @include('includes.head')
</head>

<body>

    <div id="Registration_Div">
        <div class="card">
                <a href="{{route('welcome')}}" class="close"><i class="icon-close fas fa-times"></i></a>
            <div class="card-section card-section_constrainedPadLg">
                <h2 class="h2 u-vr5x" tabindex="-1">Регистрирај се</h2>
                <form method="post" action="{{ route('register') }}" autocomplete="off">
                    @csrf
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 register-margin">
                            <span class="btn-text ">Како се викаш?</span>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="text" class="login-input{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" placeholder="Име" >
                            @if ($errors->has('first_name'))
                                <span class="red pull-right" role="alert">
                                    {{ $errors->first('first_name') }}
                                </span>
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="text" class="login-input{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" placeholder="Презиме" >
                            @if ($errors->has('last_name'))
                                <span class="red pull-right" role="alert">
                                    {{ $errors->first('last_name') }}
                                </span>
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <span class="btn-text">Каде живееш?</span>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6"> 
                            <select class="login-input{{ $errors->has('city_id') ? ' is-invalid' : '' }}" name="city_id">
                                <option selected disabled>Град</option>
                                @foreach (\App\City::all() as $city)
                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('city_id'))
                                <span class="red pull-right">{{$errors->first('city_id')}}</span> 
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <span class="btn-text">Како можеме да те исконтактираме?</span>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="text" class="login-input{{ $errors->has('telephone') ? ' is-invalid' : '' }}" name="telephone" placeholder="Телефонски број" >
                            @if($errors->has('telephone'))
                                <span class="red pull-right">{{$errors->first('telephone')}}</span> 
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">

                            <input type="email" name="email" class="login-input{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Електронска пошта" >
                            @if($errors->has('email'))
                                <span class="red pull-right">{{$errors->first('email')}}</span> 
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="password" name="password" class="login-input{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Лозинка" >
                            @if($errors->has('password'))
                                <span class="red pull-right">{{$errors->first('password')}}</span> 
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input id="password-confirm" type="password" class="login-input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password_confirmation" placeholder="Потврди ја лозинката" >
                            @if($errors->has('password'))
                                <span class="red pull-right">{{$errors->first('password')}}</span> 
                            @endif
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <input type="submit" class="login-register-btn" value="Регистрација">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    
    </div>
</body>
</html>