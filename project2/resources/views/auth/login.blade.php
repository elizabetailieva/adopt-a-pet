<!DOCTYPE html>

<html>

<head>
    @include('includes.head')
</head>

<body>
    <div id="Login_Div">
        <div class="login-content">
            <div class="login">
                <div class="login-bd" >
                        <a href="{{route('welcome')}}" class="close"><i class="icon-close fas fa-times"></i></a>
                    <div class="login-bd-inner">
                        
                        <div class="vrArray vrArray_divided m-vrArray_8x">
                            <div>
                                <h2 id="Login_Modal_Header" class="h2 u-vr10x" tabindex="-1" keyboard="true">Најави се</h2>
                            <form action="{{route('login')}}" method="post" autocomplete="off">
                                            {{csrf_field()}}
                                        <div class=" u-vr6x">
                                            <input type="email" class="login-input" name="email" placeholder="Email">
                                            @if ($errors->has('email'))
                                                <span class="red pull-right" role="alert">
                                                    {{ $errors->first('email') }}
                                                </span>
                                            @endif
                                        </div>
                                        <div class=" u-vr6x">
                                            <input type="password" class="login-input" name="password" placeholder="Лозинка">
                                            @if ($errors->has('password'))
                                                <span class="red pull-right" role="alert">
                                                   {{ $errors->first('password') }}
                                                </span>
                                            @endif
                                        </div>
                                        <div class="u-vr6x">
                                            <input type="submit" class="login-register-btn" name="submit" value="Најави се">
                                            
                                        </div>
                                    </form>
                                <div class="text-center">
                                    <a href="" class="btn-text purple purple-link">Ја заборави лозинката?</a><br>
                                    <hr class="gray-line">
                                </div>
                            </div>
                            
                            <div class="text-center">
                                <div class="text-center">
                                    <div class="btn-text gray u-vr4x">Немаш профил?</div>
                                    <a href="{{route('register')}}"><button class="btn login-register-btn">
                                   Регистрирај се</button></a>
                                    <a href="{!! url('auth/facebook') !!}" class="btn-text purple purple-link">
                                        Најави се со Facebook
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div class="login-ft" role="group" >
                    <div class="login-ft-inner">
                        <h2 class="h2 u-vr5x">Оваа страна го олеснува посвојувањето на миленичиња</h2>
                        <ul class="bulletList">
                            <li class="txt m-txt_colorWhite">Разгледај ги профилите на миленичињата кои бараат дом близу тебе.</li>
                            <li class="txt m-txt_colorWhite">Креирај го твојот профил.</li>
                            <li class="txt m-txt_colorWhite">Стапи во контакт со лица кои нудат миленичиња за вдомување.</li>
                            <li class="txt m-txt_colorWhite">Добиј експертски совети за нега на миленичиња.</li>
                            <li class="txt m-txt_colorWhite">Вкучи се и помогни во промовирање на културата за вдомување миленичиња.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>