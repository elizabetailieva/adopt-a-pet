<!DOCTYPE html>

<html>

<head>
    @include('includes.head')
</head>

<body>
    @include('includes.navbar')

    @section('content')     
    @show
    
    @include('includes.footer')
    
    <script src="{{asset('js/customjq.js')}}"></script>
    <script src="{{asset('js/petcards.js')}}"></script>
    <script src="{{asset('js/filterstyle.js')}}"></script>
    <script src="{{asset('js/filtering.js')}}"></script>
    <script src="{{asset('js/mail.js')}}"></script>
    <script src="{{asset('js/slider.js')}}"></script>

</body>



</html>