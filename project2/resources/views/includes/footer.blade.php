<footer>
    <div class="container-fluid">
        <div class="row footer-row">
            <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-1">
                <div class="footer-left">
                    <span class="footer-text">
                        Изработено од <span class="red">учесниците</span> на Aкадемијата за програмирање и Aкадемијата на маркетинг на Brainster
                    </span>
                </div>
            </div>
            <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-2">
                <div class="footer-right">
                    <ul class="footer-small list-unstyled">
                        <li><a class="footer-link" href="https://brainster.co/" target="_blank">brainster.co</a></li>
                        <li><a class="footer-link" href="http://www.brainster.io/business" target="_blank">brainster.io/business</a></li>
                        <li><a class="footer-link" href="http://codepreneurs.co/" target="_blank">codepreneurs.co</a></li>
                        <li>contact@brainster.co</li>
                        <li>+38970384728</li>
                        <li>ул. Златко Шнајдер 4а-3, 1000 Скопје</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-logo">
            <a href="https://brainster.co/" target="_blank"><img src="{{asset('./img/brainster_logo.png')}}" alt="Brainster logo"></a>
        </div>
        <div class="footer-close">
            <a href="#home"><i class="footer-icon fas fa-times"></i></a>
        </div>
    </div>
</footer>
