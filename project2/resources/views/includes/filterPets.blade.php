@foreach ($pets as $pet)  
    <div class="col-md-3 col-sm-4 pet-card-element u-vr10x">
        <div class="pet-card white-bg petFirstLayer">
            <div class="media ">
                <div class="media-image">
                    <img class="img-responsive card-image" src="{{asset('storage/'.$pet->uploads()->first()->filename)}}">
                </div>
                <div class="white-clip">
                </div>
            </div>
            <div class="card-caption">
                <p class="pet-name">{{$pet->name}}</p>
            </div>
        </div>
        <div class="pet-card white-bg petSecondLayer">
            <div class="media">
                <div class="media-image">
                    
                    <img class="img-responsive card-image" src="{{asset('storage/photos/'.$pet->uploads()->first()->resized_name)}}">
                    
                </div>
            </div>
            <div class="white-circle click-share">
                <i class="icon-share fas fa-share icon-share-close"></i>
            </div>
            <div class="card-caption">
                
                <p class="second-layer-pet-name">{{$pet->name}}</p>
                <ul class="list-unstyled pet-details detailsList">
                    <li>@if($pet->type == 'cat')
                        Маче
                        @elseif($pet->type == 'dog')
                        Куче
                        @endif
                    </li>
                    <li>
                        @if($pet->age == 'young') 
                        Младо
                        @elseif($pet->age == 'adult')
                        Возрасно
                        @elseif($pet->age == 'old')
                        Старо
                        @endif                                   
                    &#9900 
                        @if ($pet->gender == 'm')
                            Машко
                        @else
                            Женско
                        @endif
                    </li>
                    <li>{{$pet->city->name}}, Македонија</li>
                </ul>
                <p class="share-pet-name">
                    @if ($pet->gender == 'm')
                    Сподели го 
                    @else
                    Сподели ја 
                    @endif
                    {{$pet->name}}</p>
                <ul class="list-unstyled list-inline shareList">
                    <li><i class="fab fa-facebook-f"></i></li>
                    <li><i class="fab fa-twitter"></i></li>
                    <li><i class="fab fa-pinterest"></i></li>
                    <li><i class="fas fa-envelope"></i></li>
                    <li><i class="fas fa-link"></i></li>
                </ul>
            </div>
        </div>
    </div>
@endforeach
