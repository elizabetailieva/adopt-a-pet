<nav class="navbar" id="home">
    <div class="container-fluid">
            <ul class="nav navbar-nav custom-navbar">
                <a class="navbar-brand" href="/">Brainster Project</a>
                <li class="max-height font-uppercase hidden-xs hidden-sm">
                    <a id="button" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="linksNavbar" href="#linksNavbar">
                    Корисни линкови  
                    <i class="nav-icon fas fa-angle-down" id="strelce"></i></a>
                </li>
                <li>
                    <a class="nav-link hidden-sm hidden-md hidden-lg pull-right" href="" data-toggle="modal" data-target="#Navbar_Modal">
                        <i class="hamburger-button fas fa-bars"></i>
                    </a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right custom-navbar hidden-xs">
                <li class="max-height hidden-sm"><a href="{{route('addAPet')}}">Додади милениче</a></li>
                <li class="btn-li hidden-sm"><a href="{{route('listPets')}}"><button class="btn-search"><i class="fas fa-search"></i></button></a></li>
                @guest
                <li class="max-height"><a href="{{route('login')}}"><i class="nav-icon fas fa-user"></i> Најави се / Регистрирај се</a></li>
                @else
                <li class="nav-item">
                    <a id="user_avatar" class="nav-link nav-link-username" href="#"><i class="nav-icon fas fa-user"></i>
                        {{ Auth::user()->first_name }} {{ Auth::user()->last_name }} 
                    </a>
                    <div id="user_links">
                        <ul class="list-unstyled user-links-holder">
                            <li>
                                <a href="{{ route('editProfileUser') }}">Мојот профил</a>
                            </li>
                            @if (Auth::user()->role == 1)
                                <li>
                                    <a href="{{ route('adminpanel') }}">Администраторски профил</a>
                                </li>
                            @else
                            
                            @endif
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    {{ __('Одјави ме') }}
                                </a>
                            </li>
                        </ul>
                            
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>
    </div>
</nav>

<nav class="collapse navbar-dropdown" id="linksNavbar">
    <ul class="list-inline navbar-list">
        <li><a href="google.com">Oрганизации за заштита на животни</a></li>
        <li><a href="#">Други продукти на Brainster</a></li>
        <li><a href="#">Изработено од студентите на Brainster</a></li>
    </ul>
</nav>

@include('includes.navbar_modal')