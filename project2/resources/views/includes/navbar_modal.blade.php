<div id="Navbar_Modal" class="modal modal_card" role="dialog" aria-labelledby="Navbar_Modal_Header">

    <header class="mobileMenu-root-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-close fas fa-times"></i></button>
    </header>

    <div class="menuBody">
        <div class="mobileMenu-top">
            <ul class="list-unstyled" role="navigation">
                <li class=""><a class="brand-link" href="/">Brainster Project</a></li>
                <li class="mobileMenu-sub-item">
                    <a href="{{route('addAPet')}}">Додади милениче</a><i class="fas fa-angle-right"></i>
                </li>
                <li class="mobileMenu-sub-item"><a href="{{route('listPets', ['type' => 'dog'])}}">Најди куче</a><i class="fas fa-angle-right"></i>
                </li>
                <li class="mobileMenu-sub-item"><a href="{{route('listPets', ['type' => 'cat'])}}">Најди маче</a><i class="fas fa-angle-right"></i>
                </li>
            </ul>
        </div>
        <div class="mobileMenu-bottom">
        @guest
            <div class="mobilefooterLinks text-center">
                <a class="RegisterLink" href="{{route('login')}}"> Најави се / Регистрирај се</a></li>
            </div>
        @else 
            <ul class="list-unstyled">
                <li class="mobileMenu-sub-item">
                    <a href="{{ route('editProfileUser') }}">Мојот профил<i class="fas fa-angle-right"></i></a>
                </li>
                @if (Auth::user()->role == 1)
                    <li class="mobileMenu-sub-item">
                        <a href="{{ route('adminpanel') }}">Администраторски профил<i class="fas fa-angle-right"></i></a>
                    </li> 
                @endif
            </ul>
            <div class="mobilefooterLinks text-center">
                <a  class="RegisterLink" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    {{ __('Одјави ме') }}
                </a>
            </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @endguest
       </div>
    </div>
</div>