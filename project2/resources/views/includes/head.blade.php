<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="_token" content="{{csrf_token()}}" />

<title>Project 2</title>


<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.18.1/build/cssreset/cssreset-min.css">
<!-- Google fonts -->
<link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Patrick+Hand" rel="stylesheet">

<!-- Font awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

{{-- DropZone --}}
<link rel="stylesheet" href="{{asset('css/dropzone.css')}}" type="text/css">

<!-- Custom style -->
<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">

<!-- Jquery Bootstrap-JS and Dropzone-JS -->

<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{asset('js/dropzone.js') }}"></script>
<script src="{{asset('js/dropzone-config.js') }}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>

