<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function uploads(){
        return $this->hasMany(Upload::class);
    }

    public function friends()
    {
        return $this->belongsToMany(Friend::class, 'pets_friends', 'pet_id', 'friend_id')->withTimestamps();
    }
}
