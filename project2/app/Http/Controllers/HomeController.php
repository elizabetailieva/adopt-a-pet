<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\City;
use \App\Pet;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::all();
        $pets = Pet::where('approved', 1)->limit(6)->get();
        $cats = Pet::where('type', 'cat')->where('approved', 1)->with('uploads')->limit(3)->get();
        $dogs = Pet::where('type', 'dog')->where('approved', 1)->with('uploads')->limit(3)->get();
        return view('myhome', ['cities' => $cities, 'cats' => $cats, 'dogs' => $dogs, 'pets' => $pets]);
    }

    public function editProfileUser()
    {
        return view('editProfileUser');
    }

    public function myPets()
    {
        $user_id = \Auth::id();
        $pets = Pet::where('user_id', $user_id)->get();

        return view('myPets', ['pets' => $pets]);
    }

    public function addAPet(){
        return view('addAPet');
    }

    public function listPets(Request $request)
    {

        if($request->type == null){
            $pets = Pet::where('approved', 1)->with('uploads')->get();
            return view('pets', ['pets' => $pets]);
        }

        $pets = Pet::where('approved', 1)->where('type', $request->type)->with('uploads')->get();
        return view('pets', ['pets' => $pets]);
    }

    public function filterPetsByCity(Request $request)
    {
        $filter = $request->zip_code;

        if(is_numeric($filter)){
        //User entered zip code
            $pets = Pet::whereHas('city', function($query) use($filter){
                $query->where('zip_code', $filter);
            })->get();
    
            return view('pets', ['pets' => $pets]);
        }

        //User entered City Name

        $pets = Pet::whereHas('city', function($query) use($filter){
            $query->where('name', $filter);
        })->get();

        return view('pets', ['pets' => $pets]);
        
    }




}
