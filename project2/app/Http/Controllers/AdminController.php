<?php

namespace App\Http\Controllers;
use App\Http\Middleware\CheckRole;
use Illuminate\Http\Request;
use App\User;
use App\Pet;

class AdminController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('check_role');
    }
    
     public function showAdminPanel() {

          $pets = Pet::all();

        return view('admin', ['pets' => $pets]);
         
     
    }

       public function updateStatus(Request $request, $id) {


        $pet = Pet::find($request->id);

        return view('updateStatus', ['pet' => $pet]);

    }

    public function approvePost(Request $request, $id) {

        $pet = Pet::find($id);

		if($pet) {
     	   $pet->approved = true;
           $pet->save();
		}

        return redirect()->route('adminpanel');
    	
    	
    }

    public function deny(Request $request, $id) {

    	 $pet = Pet::find($id);

        if($pet) {
     	   $pet->approved = false;
           $pet->save();
		}
		return redirect()->route('adminpanel');
    }

     public function delete(Request $request, $id) {

     

        $pet = Pet::find($id);

        $pet->delete();

        return redirect()->route('adminpanel');
        
    }

}
