<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Pet;
use App\City;
use App\Mail\ContactUser;
use Symfony\Component\HttpFoundation\Response;

class MailController extends Controller
{
    public function inquirePet(Request $request){

        $to = User::findOrFail($request->petOwnerID);
        
        $recipient_email = $to->email;
        $recipient_name = $to->first_name;

        $sender_email = $request->email;
        $sender_name = $request->firstName." ".$request->lastName;
        $telephone = $request->telephone;
        $city = City::find($request->cityID);
        $city_name = $city->name;
        
        $pet = Pet::where('id', $request->petID)->with('uploads')->first();
        $pet_name = $pet->name;

        $image = $pet->uploads()->first()->resized_name;
        
       
        $content = $request->message;


        \Mail::send('emails.mail', [
            'recipient_name' => $recipient_name,
            'content' => $content, 
            'pet_name' => $pet_name, 
            'sender_name' => $sender_name,
            'city_name' => $city_name, 
            'telephone' => $telephone,
            'sender_email' => $sender_email,
            'image' => $image], function ($message) use ($sender_email, $sender_name, $recipient_email)
        {
            $message->from($sender_email, $sender_name);
            $message->to($recipient_email);

        });

        return response()->json(['message' => 'Request completed']);

    }

}
