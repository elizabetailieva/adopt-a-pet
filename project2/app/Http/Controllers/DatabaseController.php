<?php

namespace App\Http\Controllers;

use App\User;
use App\Pet;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Friend;

class DatabaseController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'city_id' => 'required|integer',
            'telephone' => 'required|string',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $id = \Auth::id();
        $user = User::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->city_id = $request->city_id;
        $user->telephone = $request->telephone;
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->route('home');
    }

    public function savePet(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'type' => 'required|string|max:255',
            'city_id' => 'required',
            'age' => 'required',
            'size' => 'required',
            'gender' => 'required',
            'description' => 'required|string|max:2000'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $pet = new Pet;
        $pet->user_id = \Auth::id();
        $pet->name = $request->name;
        $pet->type = $request->type;
        $pet->city_id = $request->city_id;
        $pet->age = $request->age;
        $pet->size = $request->size;
        $pet->gender = $request->gender;
        $pet->description = $request->description;
        $pet->save();

        $friends = $request->good_with;

        foreach($friends as $friend_id) {
            $friend = Friend::find($friend_id);
            $pet->friends()->save($friend);
        }
        
        return view('addPhotosPet')->with(['pet_id' => $pet->id, 'msg' => 'success']);
    }


}
