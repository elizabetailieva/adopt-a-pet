<?php

namespace App\Http\Controllers;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

use App\City;
use App\Pet;
use Illuminate\View\FileViewFinder;

class GeneralController extends Controller
{
    public function welcome()
    {
        $cities = City::all();
        $pets = Pet::where('approved', 1)->limit(6)->get();
        $cats = Pet::where('type', 'cat')->where('approved', 1)->limit(3)->get();
        $dogs = Pet::where('type', 'dog')->where('approved', 1)->limit(3)->get();
        return view('myhome', ['cities' => $cities, 'cats' => $cats, 'dogs' => $dogs, 'pets' => $pets]);
    }


    public function petID(Request $request){
        $cities = City::all();
        $pet = Pet::where('id', decrypt($request->id))->with('uploads')->with('user')->first();      

        return view('petDetails', ['pet' => $pet, 'cities' => $cities]);
    }

}
