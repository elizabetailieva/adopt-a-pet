<?php

namespace App\Http\Middleware;

use Closure;


class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
      public function handle($request, Closure $next)
    {
        $user_role_id = \Auth::user()->role;
        // dd($user_role_id);
        if ($user_role_id == 1) {
            // he is admin
            return $next($request);

        } 

        return redirect()->route('home');
    }
}